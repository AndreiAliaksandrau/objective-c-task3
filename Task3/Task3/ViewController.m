//
//  ViewController.m
//  Task3
//
//  Created by Андрей Александров on 3/23/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
- (IBAction)clickButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickButton:(id)sender {
    self.labelField.text = [NSString stringWithFormat:@"%@ %@", self.labelField.text, self.textField.text];
}
@end
