//
//  AppDelegate.h
//  Task3
//
//  Created by Андрей Александров on 3/23/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

